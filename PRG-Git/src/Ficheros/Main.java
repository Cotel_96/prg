import java.io.*;
import java.util.*;

public class Main{

	public static void main(String[] args) {

		File fileP = new File("Hola.txt");
		Scanner teclado = new Scanner(System.in);
		ObjectOutputStream os;

		try {
			os = new ObjectOutputStream(new FileOutputStream(fileP));
			System.out.print("Introduce una frase a guardar en un objeto: ");
			os.writeObject(new PruebaBinario(teclado.nextLine()));
			os.close();
		} catch (IOException e){
			e.printStackTrace();
		}


		File file = new File("Hola.txt");
		PruebaBinario p = null;
		ObjectInputStream is;

		try {
			is = new ObjectInputStream(new FileInputStream(file));
			do {
				p = (PruebaBinario) (is.readObject());
				System.out.println(p);
			} while(true);
		} catch (EOFException e){ 
			System.out.println("\nFin del archivo");
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}