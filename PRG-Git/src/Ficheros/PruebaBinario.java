import java.io.*;

public class PruebaBinario implements Serializable {
	
	private String frase;

	public PruebaBinario(String frase){
		this.frase = frase;
	}

	public String toString(){
		return this.frase;
	}

}