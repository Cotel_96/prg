import java.util.*;
import java.io.*;

public class Excepciones{

	public static void main(String[] args) throws IOException{ // <------
		File f = null;
		Scanner archivo = null;
		try{
			f = new File("Hola.txt");
			archivo = new Scanner(f);
		} catch (IOException e){
			e.printStackTrace();
		}

		while(archivo.hasNextLine()){
			String linea = archivo.nextLine();
			System.out.println(linea);

			if(linea.equals("Error")){
				throw new IllegalArgumentException("El archivo tiene un Error"); // Este tipo de excepciones no necesita throws en el metodo
				// throw new IOException ("El archivo tiene un Error"); // Para lanzar excepciones de ciertos tipos se necesita que el metodo lance throws
			}
		}
		
	}


}