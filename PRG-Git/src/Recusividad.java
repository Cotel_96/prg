import java.math.BigInteger;

public class Recusividad{

	public static long fibonacci(int i){
		if(i<=1) return i;
		else return fibonacci(i-1) + fibonacci(i-2);
	}

	public static int buscaNum(int[] a,int buscado,int i){
		if(a[i-1]==buscado){
			return i;
		}else{
			return buscaNum(a,buscado,i+1);
		}
	}

	public static int sumaIterativa(int aSumar, int veces){
		int x = 0;
		for(int i = 0; i<veces;i++){
			x += aSumar;
		}
		return x;
	}

	public static int sumaRecursiva(int aSumar, int veces){
		if(veces == 0) return 0;
		else return aSumar + sumaRecursiva(aSumar,veces-1);
	}

	public static long aBinario(int n){
		if(n<=1) return n;
		else return n%2+aBinario(n/2)*10;
	}

	public static int maxiArray(int[] a,int i){
		if(a.length==i) return a[i-1];
		else if(a[i]>maxiArray(a,i+1)) return a[i];
		else return maxiArray(a,i+1);
	}

	public static void main(String[] args) throws Exception{
			long start = System.currentTimeMillis();
			System.out.println(sumaRecursiva(2,10000));
			long elapsed = System.currentTimeMillis() - start;
			System.out.println("Tiempo que ha tardado: " + elapsed + "ms");
	}
}