package lineales;

import java.util.NoSuchElementException;

public class ListaEnlazada {
	
	protected NodoInt primero, PI, antPI;
	protected int talla;
	
	/** Constructor ListaEnlazada
	 * 
	 */
	public ListaEnlazada(){
		primero = null;
		PI = null;
		antPI = null;
		talla = 0;
	}
	
	/** Indica si el punto de inter�s est� a la derecha del todo
	 * 
	 * @return true si el punto de inter�s est� a la derecha del todo
	 */
	public boolean esFin(){
		return PI == null;
	}
	
	/** Devuelve true si la ListaEnlazada no tiene elementos
	 * 
	 * @return true si la lista est� vac�a
	 */
	public boolean esVacia(){
		return talla == 0;
	}
	
	/** Devuelve el tama�o de la ListaEnlazada
	 * 
	 * @return int
	 */
	public int talla(){
		return talla;
	}
	
	/** Situa el punto de inter�s al principio de la lista
	 * 
	 */
	public void inicio(){
		PI = primero;
		antPI = null;
	}
	
	/** Inserta un elemento en la lista delante del punto de inter�s
	 * 
	 * @param elemento Int Entero que se quiere insertar
	 */
	public void insertar(int elemento){
		if(primero == PI){
			primero = new NodoInt(elemento, primero);
			antPI = primero;
		} else {
			antPI.siguiente = new NodoInt(elemento, PI);
			antPI = antPI.siguiente;
		}
		talla++;
	}
	
	/** Elimina y devuelve el elemento del punto de inter�s sin moverlo
	 *  Si el punto de inter�s esta al final lanza la excepci�n NoSuchElementException
	 * @throws NoSuchElementException
	 * @return int
	 */
	public int eliminar(){
		int aux = PI.dato;
		if (PI == null) throw new NoSuchElementException("Final de la Lista");
		else if(primero == PI){
			primero = primero.siguiente;
		} else {			
			antPI.siguiente = PI.siguiente;
		}
		PI = PI.siguiente;
		talla--;
		return aux;		
	}
	
	/** Consulta el elemento que est� en el punto de inter�s
	 *  Si el punto de inter�s est� al final lanza la excepci�n NoSuchElementException
	 * @throws NoSuchElementException
	 * @return int
	 */
	public int recuperar(){
		if (PI == null) throw new NoSuchElementException("Final de la Lista");
		return PI.dato;
	}
	
	/** Mueve el punto de inter�s una posici�n a la derecha
	 *  Si el punto de inter�s est� al final lanza la excepci�n NoSuchElementException
	 * @throws NoSuchElementException
	 */
	public void siguiente(){
		if (PI == null) throw new NoSuchElementException("Final de la Lista");
		antPI = PI;
		PI = PI.siguiente;
	}
	
	/** Cuenta el numero de elementos iguales a el par�metro introducido
	 * 
	 * @param elemento int Dato del que se quieren contar iguales
	 * @return int
	 */
	public int contarIgualesQue(int elemento){
		int res = 0;
		for(NodoInt aux = primero; aux != null; aux = aux.siguiente){
			if (aux.dato == elemento) res++;
		}
		return res;
	}
	
	/** Elimina de la lista todos los elementos iguales al par�metro introducido
	 *  Devuelve la cantidad de elementos eliminados
	 * 
	 * @param elemento int Dato que se quiere eliminar de la lista
	 * @return int
	 */
	public int eliminarIgualesQue(int elemento){
		int res = 0;
		NodoInt aux = primero;
		for(int i = 0; aux != null; i++){
			if (aux.dato == elemento){
				inicio();
				for(int j=0; j<i; j++){
					siguiente();
				}
				eliminar();
				res++;
				i = 0;
			}
			aux = aux.siguiente;
		}
		return res;
	}
	
	public String toString(){
		String res = "";
		for(NodoInt aux = primero; aux != null; aux = aux.siguiente){
			res += aux.dato + " ";
		}
		return res;
	}
	
}
