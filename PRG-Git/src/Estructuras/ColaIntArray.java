package lineales;

import java.util.NoSuchElementException;

/** Clase ColaIntArray
 *  Cola de Enteros implementada con Arrays
 * @author Cotelo
 *
 */
public class ColaIntArray {
	
	protected int primero, ultimo, numElem;
	protected int[] array;
	protected final int TALLA_INICIAL = 10;
	
	public ColaIntArray(){
		array = new int[TALLA_INICIAL];
		numElem = 0;
		primero = 0;
		ultimo = 0;
	}
	
	public boolean esVacia(){
		return numElem == 0;
	}
	
	public int talla(){
		return numElem;
	}
	
	/** Metodo que devuelve el primer elemento situado en la cola
	 * PRECONDICION: La cola no puede estar vacia, en caso contrario
	 * se lanza la exception NoSuchElementException
	 * 
	 * @return Entero en la primera posicion de la cola
	 */
	public int primero(){
		if (numElem == 0) throw new NoSuchElementException("Cola Vacia");
		else return array[primero];
	}
	
	protected int siguiente(int actual){
		if(actual == array.length-1){
			return 0;
		} else {
			return actual+1;
		}
	}
	
	protected void duplicarArray(){
		int[] nuevo = new int[array.length*2];
		ultimo = primero;
		primero = 0;
		for(int i=0; i<array.length;){
			nuevo[i++] = array[ultimo];
			ultimo = siguiente(ultimo);
		}
		ultimo = numElem-1;
		array = nuevo;
	}
	
	public void encolar(int elemento){
		if(numElem == array.length) duplicarArray();					
		numElem++;
		array[ultimo] = elemento;
		ultimo = siguiente(ultimo);						
	}
	
	public int desencolar(){
		if (numElem == 0){
			throw new NoSuchElementException("Cola Vacia");
		} else {
			int aux = array[primero];
			primero = siguiente(primero);
			numElem--;
			return aux;			
		}
	}
	
	public String toString() {
		String res = "[";
		if(numElem == 0) throw new NoSuchElementException("Pila Vacia");
		for (int i=primero; i<ultimo;){
			if (i != ultimo -1) res += array[i] + ", ";
			else res += array[i];
			i = siguiente(i);
		}
		res += "]";
		return res;
	}
	
	public boolean equals(Object o){
		boolean res = false;
		if (o instanceof ColaIntArray){
			ColaIntArray objeto = (ColaIntArray) o;
			if (objeto.talla() == numElem){
				if(objeto.primero == primero){
					for (int i=primero; i<ultimo;){
						if(objeto.array[i] == array[i]){
							res = true;
							i = siguiente(i);
						} else return false;
					}
				}
			}
		}
		return res;
	}
	
	
	
}
