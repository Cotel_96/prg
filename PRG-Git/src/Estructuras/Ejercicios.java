package lineales;

public class Ejercicios {
	
	public static int borrarBasePila(PilaEnlazada p){
		PilaEnlazada aux = new PilaEnlazada();
		int talla = p.talla();
		for(int i=talla; i>0; i--){
			aux.apilar(p.desapilar());
		}
		int res = aux.desapilar();
		for(int i=0; i<talla-1; i++){
			p.apilar(aux.desapilar());
		}
		return res;
	}
	
	public static int borrarBasePilaRecursivo(PilaEnlazada p){
		if(p.talla() <= 1){
			return p.desapilar();
		} else {
			int noUltimo = p.desapilar();
			int res = borrarBasePilaRecursivo(p);
			p.apilar(noUltimo);
			return res;
		}
	}
	
	public static void invierteCola(ColaEnlazada c){ 				// Se puede hacer mas facil con una pila auxiliar, pero no funciona por ser estatico
		int talla = c.talla();
		int[] numeros = new int[talla];
		
		for(int i=0; i<talla; i++){
			numeros[i] = c.desencolar();
		}
		
		for(int i=talla-1; i>=0; i--){
			c.encolar(numeros[i]);
		}
	}
	
	public static void invierteColaRecursivo(ColaEnlazada c){
		if(!c.esVacia()){
			int aux = c.desencolar();
			invierteColaRecursivo(c);
			c.encolar(aux);
		}
	}
	
	public static boolean esSombrero(PilaEnlazada a, PilaEnlazada b){
		boolean res = true;
		NodoInt auxA = a.cima;
		NodoInt auxB = b.cima;
		
		if(a.talla()>b.talla()) return false;
		else{
			for(int i=0; i<a.talla(); i++){
				if(auxA.dato != auxB.dato){
					res = false;
				}
				auxA = auxA.siguiente;
				auxB = auxB.siguiente;
			}
		}
		return res;
		
	}
	
	public static boolean esSombreroRecursivo(PilaEnlazada a, PilaEnlazada b){
		boolean res = false;
		if(a.esVacia()) return true;
		else{
			if(a.talla()>b.talla()){
				int v1 = a.desapilar();
				int v2 = b.desapilar();
				res = (v1==v2) && esSombreroRecursivo(a,b);
				a.apilar(v1);
				b.apilar(v2);
			}
		}
		return res;
	}
	
	public static boolean estaOrdenada(ListaEnlazada p){
		NodoInt aux = p.primero;
		
		for(int i=0; i<p.talla()-1; i++){
			int orden = aux.dato;
			if(orden > aux.siguiente.dato) return false;
			aux = aux.siguiente;
		}
		return true;
	}
	
	public static void insertarOrdenado(ListaEnlazada l, int elemento){ 			// No funciona por ser estatico.
		l.inicio();
		for(int i=0; i<l.talla()-1; i++){
			if(l.PI.dato <= elemento){
				l.siguiente();
			} else {
				l.insertar(elemento);
			}
		}
	}

	public static void main(String[] args) {
		PilaEnlazada pila = new PilaEnlazada();
		PilaEnlazada b = new PilaEnlazada();
		for(int i=1; i<=5; i++){
			pila.apilar(i);
		}
		for(int i=0; i<=5; i++){
			b.apilar(i);
		}
		
		borrarBasePilaRecursivo(pila);
		System.out.println(pila);
		
	}

}
