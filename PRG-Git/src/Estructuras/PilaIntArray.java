package lineales;

import java.util.Arrays;
import java.util.NoSuchElementException;

/** Clase PilaIntArray
 *  Pila de enteros implementada con arrays
 * @author Cotelo
 *
 */

public class PilaIntArray {
	
	protected int[] array;
	protected int numElem;
	private final int TALLA_INICIAL = 10;

	public PilaIntArray(){
		array = new int[TALLA_INICIAL];
		numElem = 0;
	}	
	
	public boolean esVacia(){
		return numElem == 0;
	}
	
	public int talla(){
		return numElem;
	}
	
	public int cima(){
		if (numElem == 0) throw new NoSuchElementException("Pila Vacia");
		else return array[numElem - 1];
	}
	
	public void apilar(int elemento){
		if(numElem == array.length){
			int[] aux = array;
			array = new int[aux.length * 2];
			for(int i = 0; i<numElem; i++){
				array[i] = aux[i];
			}
		}
		array[numElem] = elemento;
		numElem++;
	}
	
	public int desapilar(){
		if (numElem == 0) throw new NoSuchElementException("Pila Vacia");
		else {
			int dato = array[--numElem];
			return dato;
		}		
	}
	
	/** Devuelve la cantidad de elementos en la estructura que son iguales a un dato introducido
	 * 
	 * @param elemento int Entero del que se quiere contar las veces que aparece en la estructura
	 * @return int
	 */
	public int contarIgualesQue(int elemento){
		int res = 0;
		if (numElem == 0) throw new NoSuchElementException("Pila Vacia");
		for(int i=0; i<numElem; i++){
			if (array[i] == elemento) res++;
		}
		return res;
	}
	
	public String toString(){
		String res = "";
		for(int i = numElem-1; i>=0; i--){
			res += array[i] + "\n";
		}
		return res;
	}
	
	public boolean equals(Object o){
		boolean res = false;
		if(o instanceof PilaIntArray){
			if(((PilaIntArray) o).talla() == numElem){
				res = Arrays.equals(((PilaIntArray) o).array, this.array);				
			}
		}
		return res;
	}
}