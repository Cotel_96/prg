package lineales;

import java.util.NoSuchElementException;

/** Clase PilaEnlazada
 *  Pila de enteros implementada con NodoInt
 * @author Cotelo
 *
 */
public class PilaEnlazada {
	
	protected NodoInt cima;
	protected int numElem;
	
	public PilaEnlazada(){
		cima = null;
		numElem = 0;
	}
	
	public boolean esVacia(){
		return numElem == 0;
	}
	
	public int talla(){
		return numElem;
	}
	
	public int cima(){
		if (numElem == 0) throw new NoSuchElementException("Pila Vacia");
		else return cima.dato;		
	}
	
	public void apilar(int elemento){
		cima = new NodoInt(elemento, cima);
		numElem++;		
	}
	
	public int desapilar(){
		if(numElem == 0) throw new NoSuchElementException("Pila Vacia");
		else {
			int dato = cima.dato;
			numElem--;
			cima = cima.siguiente;
			return dato;
		}
	}
	
	/** Devuelve la cantidad de elementos en la estructura que son iguales a un dato introducido
	 * 
	 * @param elemento int Entero del que se quiere contar las veces que aparece en la estructura
	 * @return int
	 */
	public int contarIgualesQue(int elemento){
		int res = 0;
		if(numElem == 0) throw new NoSuchElementException("Pila Vacia");		
		else{
			NodoInt aux = cima;
			for(int i=numElem-1; i>=0; i--){
				if(aux.dato == elemento) res++;
				aux = aux.siguiente;
			}
		}
		return res;
	}
	
	/** Elimina los elementos iguales al introducido
	 *  Devuelve el numero de elementos eliminados
	 * @param elemento int Dato que se desea eliminar
	 * @return int
	 */
	public int eliminarIgualesQue(int elemento){
		int cont = 0;
		if (!esVacia()){
			int aux = desapilar();
			cont += eliminarIgualesQue(elemento);
			if (aux != elemento) apilar(aux);
		}
		return cont;
	}
	
	/** Devuelve true si la pila esta contenida en la misma posicion
	 *  en las primeras posiciones de otra pila.
	 * @param b PilaEnlazada Pila de la que se quiere averiguar si this es sombrero
	 * @return boolean
	 */
	public boolean esSombrero(PilaEnlazada b){
		boolean res;
		if (b.numElem==0) res = true;
		else if(b.talla()>this.talla()){
			res = false;
		}
		else{
			NodoInt aux1 = this.cima;
			NodoInt aux2 = b.cima;
			while(aux2 != null && aux1.dato == aux2.dato){
				aux1 = aux1.siguiente;
				aux2 = aux2.siguiente;
			}
			if(aux2==null){
				res = true;
			} else {
				res = false;
			}
		}
		return res;
	}
	
	public String toString(){
		String res = "";
		NodoInt aux = cima;
		if(numElem == 0) throw new NoSuchElementException("Pila Vacia");
		for(int i=numElem-1; i>=0; i--){
			res += aux.dato + "\n";
			aux = aux.siguiente;
		}
		return res;
	}
	
	public boolean equals(Object o){		
		boolean res = false;
		if(o instanceof PilaEnlazada){
			if(((PilaEnlazada)o).talla() == numElem){
				res = ((PilaEnlazada)o).toString().equals(this.toString());
			}
		}
		return res;
	}
}
