package lineales;

import javax.swing.JOptionPane;

public class Main {
	
	public static int BusquedaSecuencia (NodoInt sec, int d){
		NodoInt aux = sec;
		int res = 0;
		while(aux != null){
			if(aux.dato == d) return res;
			else{
				res++;
				aux = aux.siguiente;
			}
		}
		return -1;
	}
	
	public static void saturar (NodoInt sec, int maximo){
		NodoInt aux = sec;
		while(aux != null){
			if(aux.dato>maximo) aux.dato = maximo;
			aux = aux.siguiente;
		}
	}
	
	/** M�todo para insertar un int al final de la estructura
	 * 
	 * @param sec = NodoInt para tomar como punto inicial
	 * @param d = Int a insertar
	 * @return NodoInt que contiene el dato introducido
	 */
	public static NodoInt insertarAlFinal (NodoInt sec, int d){
		NodoInt aux = sec;
		NodoInt ant = null;
		while(aux != null){
			ant = aux;
			aux = aux.siguiente;
		}
		if (ant == null){ 									// Estructura vac�a
			return new NodoInt(d);
		} else {  											// ant est� en el �ltimo elemento
			ant.siguiente = new NodoInt(d);
			return sec;
		}
	}
	
	/** M�todo para insertar un int al principio de la estructura
	 * 
	 * @param sec = NodoInt que hace referencia al primer elemento
	 * @param d = Int a insertar
	 * @return NodoInt con el elemento insertado
	 */
	public static NodoInt insertarAlPrincipio (NodoInt sec, int d){
		return new NodoInt(d,sec);
	}
	
	/** M�todo que inserta un entero en una secuencia ordenada
	 * PRECONDICI�N: La secuencia ha de estar ordenada
	 * @param sec = NodoInt a partir del cual se hace la busqueda para la inserci�n
	 * @param d = Entero a introducir
	 * @return NodoInt resultante
	 */
	public static NodoInt insertarOrdenado (NodoInt sec, int d){
		NodoInt aux = sec;
		NodoInt ant = null;
		NodoInt nuevo = new NodoInt(d);
		while(aux != null && aux.dato < d){
			ant = aux;
			aux = aux.siguiente;
		}
		nuevo.siguiente = aux;
		if(ant == null){									// La secuencia est� vacia o el elemento es mayor
			sec=nuevo;
		} else {											// aux se ha salido o apunta a un elemento mayor
			ant.siguiente = nuevo;
		}
		return sec;
		
	}
	
	public static void main(String[] args){
		PilaEnlazada prueba = new PilaEnlazada();		
		prueba.apilar(1);
		JOptionPane.showMessageDialog(null, prueba.toString());
		prueba.apilar(2);
		prueba.apilar(15);
		prueba.apilar(15);
		prueba.apilar(15);
		//JOptionPane.showMessageDialog(null,prueba.contarIgualesQue(15));
		ColaIntArray auxiliar = new ColaIntArray();
		//System.out.println(prueba.equals(auxiliar));
		System.out.println(prueba);
		System.out.println(prueba.eliminarIgualesQue(15));
		System.out.println(prueba.toString());
		auxiliar.encolar(1);
		auxiliar.encolar(2);
		auxiliar.encolar(3);
		auxiliar.encolar(15);
		//System.out.println(prueba.equals(auxiliar));
		auxiliar.desencolar();
		auxiliar.encolar(5);
		//JOptionPane.showMessageDialog(null,auxiliar.toString());
		//System.out.println(prueba.equals(auxiliar));
	}
}
