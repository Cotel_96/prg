package lineales;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ListaIntOrdenada extends ListaEnlazada implements Serializable{
	
	public ListaIntOrdenada(){
		super();
	}
	
	/** Inserta un elemendo de forma ordenada en la Lista
	 * 
	 * @param e Int: Elemento a insertar
	 */
	@Override
	public void insertar(int e){
		if(talla==0){
			super.insertar(e);
		} else {
			NodoInt actual = this.primero;
			NodoInt anterior = null;
			while(actual != null || actual.dato<e){
				anterior = actual;
				actual = actual.siguiente;
			}
			
			if(anterior == null){
				this.primero = new NodoInt(e, primero);
				// Si estamos insertando en el primero y ese
				// era el PI hay que modificar antPI
				if(this.antPI == null) this.antPI = primero;
			} else {
				anterior.siguiente = new NodoInt(e, actual);
				// Si se inserta ente antPI y PI, se ha de avanzar antPI
				if(antPI.siguiente != PI) this.antPI = this.antPI.siguiente;
			}
			
			this.talla++;
		}
	}
	
	/** Comprueba si un elemento existe dentro de la Lista
	 * 
	 * @param e Int: Elemento a comprobar
	 * @return boolean
	 */
	public boolean contains(int e){
		boolean encontrado = false;
		NodoInt aux = primero;
		
		while(aux != null && aux.dato != e){
			aux = aux.siguiente;
		}
		encontrado = (aux != null);
		return encontrado;
	}
	
	/** Elimina un elemento de la Lista
	 * 
	 * @param e Int: Elemento a eliminar
	 * @throws NoSuchElementException: El elemento no se encuentra en la lista
	 * @return int Elemento eliminado
	 */
	public int eliminar(int e) throws NoSuchElementException {
		if(!contains(e)) throw new NoSuchElementException("Elemento no encontrado!");
		else{
			if(this.PI != null && e == this.PI.dato) return super.eliminar();
			else {
				NodoInt actual = this.primero;
				NodoInt anterior = null;
				while(actual != null && actual.dato<e){
					anterior = actual;
					actual = actual.siguiente;
				}
				if(actual == this.primero){
					primero = actual.siguiente;
				} else {
					actual = actual.siguiente;
					anterior.siguiente = actual;
					// Si lo que hemos borrado es antPI lo cambiamos a anterior
					if(antPI == actual) antPI = anterior;
				}
				talla--;
				return actual.dato;
			}
		}
	}
	
	/** Guarda la lista en un fichero de texto plano
	 *  Cada numero en una linea
	 */
	public void guardarTexto(){
		System.out.println("Introduce un nombre de fichero: ");
		System.out.print("> ");
		Scanner teclado = new Scanner(System.in);
		String nombre = teclado.nextLine();
		
		try {
			File archivo = new File(nombre + ".txt");
			PrintWriter pw = new PrintWriter(archivo);
			// pw.println(super.toString());
			NodoInt auxiliar = primero;
			while(auxiliar != null){
				pw.println(auxiliar.dato);
				auxiliar = auxiliar.siguiente;
			}
			pw.close();
			teclado.close();
			
		} catch (FileNotFoundException e){
			System.out.println(e.getMessage());
		}
	}
	
	/** Guarda la lista en un archivo binario
	 * 
	 */
	public void guardarBinario(){
		System.out.println("Introduce un nombre de fichero: ");
		System.out.print("> ");
		Scanner teclado = new Scanner(System.in);
		String nombre = teclado.nextLine();
		
		try {
			File archivo = new File(nombre + ".bin");
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(archivo));
			oos.writeObject(this);
			oos.close();
			teclado.close();
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}
	
	public static ListaIntOrdenada recuperarTexto(){
		System.out.println("Introduce un nombre de fichero: ");
		System.out.print("> ");
		Scanner teclado = new Scanner(System.in);
		String nombre = teclado.nextLine();
		
		try {
			File archivo = new File(nombre + ".txt");
			Scanner scanner = new Scanner(archivo);
			ListaIntOrdenada aux = new ListaIntOrdenada();
			while(scanner.hasNext()){
				aux.insertar(scanner.nextInt());
			}
			scanner.close();
			teclado.close();
			return aux;
			
		} catch (FileNotFoundException e){
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public static ListaIntOrdenada recuperarBinario(){
		System.out.println("Introduce un nombre de fichero: ");
		System.out.print("> ");
		Scanner teclado = new Scanner(System.in);
		String nombre = teclado.nextLine();
		
		try {
			File archivo = new File(nombre + ".bin");
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(archivo));
			ListaIntOrdenada aux = (ListaIntOrdenada) ois.readObject();
			ois.close();
			teclado.close();
			return aux;
		} catch (EOFException e){
			System.out.println("Fin del archivo");
		} catch (IOException | ClassNotFoundException | ClassCastException e){
			System.out.println(e.getMessage());
		}
		
		return null;
	}
	
	
	
}
