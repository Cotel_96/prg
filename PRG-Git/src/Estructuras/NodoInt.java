package lineales;

import java.io.Serializable;

/** Estructura de datos Nodo
 *  Nodo que guarda un dato de tipo Integer. 
 * @author Cotelo
 *
 */

public class NodoInt implements Serializable{
	
	int dato; 					// Dato que guarda el nodo.
	NodoInt siguiente; 			// Siguiente nodo al que apunta.	
	
	/** Constructor
	 * Crea un nodo con un dato y una referencia vacia.
	 * @param dato = Dato a guardar.
	 */
	public NodoInt(int dato) {
		this.dato = dato;
		this.siguiente = null;
	}
	
	/** Constructor
	 * Crea un nodo con un dato y una referencia al siguiente nodo.
	 * @param dato = Dato a guardar.
	 * @param s = Nodo al que apunta.
	 */
	public NodoInt(int dato, NodoInt s){
		this.dato = dato;
		this.siguiente = s;
	}	
	
}
