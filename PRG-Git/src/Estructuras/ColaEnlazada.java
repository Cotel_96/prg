package lineales;

import java.util.NoSuchElementException;

/** Clase ColaEnlazada
 *  Cola de Enteros implementada con NodoInt
 * @author Cotelo
 *
 */
public class ColaEnlazada {

	protected int numElem;
	protected NodoInt primero;
	protected NodoInt ultimo;
	
	public ColaEnlazada(){
		primero = ultimo = null;
		numElem = 0;
	}
	
	public boolean esVacia(){
		return numElem == 0;
	}
	
	public int talla(){
		return numElem;
	}
	
	/** Metodo que devuelve el primer elemento situado en la cola
	 * PRECONDICION: La cola no puede estar vacia, en caso contrario
	 * se lanza la exception NoSuchElementException
	 * 
	 * @return Entero en la primera posicion de la cola
	 */
	public int primero(){
		if (numElem == 0){
			throw new NoSuchElementException("Cola Vacia");
		} else return primero.dato;
	}
	
	/** Metodo para insertar en la cola
	 * 
	 * @param elemento = Entero a insertar
	 */
	public void encolar(int elemento){
		if (numElem==0){
			primero = ultimo = new NodoInt(elemento);
		} else {
			ultimo.siguiente = new NodoInt(elemento);
			ultimo = ultimo.siguiente;
		}
		numElem++;
	}
	
	/** Metodo para quitar el ultimo elemento de la cola
	 * PRECONDICION: La cola no puede estar vacia.
	 * 
	 * @return Elemento retirado
	 */
	public int desencolar(){
		if (numElem == 0) throw new NoSuchElementException("Pila Vacia");
		else {
			int dato = primero.dato;
			numElem--;
			primero = primero.siguiente;			
			if (primero == null) {
				ultimo = null;
			}
			return dato;
		}
	}
	
	/** Cuenta el n�mero de elementos iguales al par�metro introducido
	 * 
	 * @param elemento int Dato que se desea contar en la cola.
	 * @return int
	 */
	public int contarIgualesQue(int elemento){
		int resultado = 0;
		NodoInt aux = primero;
		for(int i = 0; i < numElem; i++){
			if (aux.dato == elemento) resultado++;
			aux = aux.siguiente;
		}
		return resultado;
	}
	
	public int eliminarIgualesQue(int elemento){
		int cuenta = 0;
		if(!esVacia()){
			int aux = desencolar();
			cuenta += eliminarIgualesQue(elemento);
			if(aux != elemento) encolar(aux);			
		}
		return cuenta;
	}
	
	public String toString(){
		String res = "[";
		NodoInt aux = primero;
		if(numElem == 0) throw new NoSuchElementException("Pila Vacia");
		for (int i = 0; i<numElem; i++){
			if(i != numElem -1) res += aux.dato + ", ";
			else res += aux.dato;
			aux = aux.siguiente;
		}
		res += "]";
		return res;
	}
	
	public boolean equals(Object o){
		boolean res = false;
		if (o instanceof ColaEnlazada){
			ColaEnlazada objeto = (ColaEnlazada) o;
			if(objeto.talla() == this.numElem){
				NodoInt local = primero;
				NodoInt exterior = objeto.primero;
				for(int i = 0; i<numElem; i++){
					if(local.dato == exterior.dato){
						res = true;
						local = local.siguiente;
						exterior = exterior.siguiente;
					} else {
						return false;
					}
				}
			}
		}
		return res;
	}
	
	
	
}
