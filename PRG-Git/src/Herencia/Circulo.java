public class Circulo extends Figura{

	private double radio;

	public Circulo(String nombre, String color, int x, int y, double radio){
		super(nombre,color,x,y);
		this.radio = radio;
	}

	public double getRadio() {return this.radio;}

	@Override
	public double area(){
		return Math.PI*radio*radio;
	}

	@Override
	public double perimetro(){
		return 2*Math.PI*radio;
	}

	public String toString(){
		return super.toString() + " Radio: " + this.radio;
	}

	public boolean equals(Object o){
		if(o instanceof Circulo){
			Circulo otro = (Circulo)o;
			return super.equals(o) &&
					this.radio == otro.getRadio();
		}else return false;
	}

}