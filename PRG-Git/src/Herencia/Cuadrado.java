public class Cuadrado extends Rectangulo{

	public Cuadrado(String nombre, String color, int x, int y, double lado){
		super(nombre,color,x,y,lado,lado);
	}

	@Override
	public double perimetro(){
		return super.base * 4;
	}

	public String toString(){
		return "Nombre: " + super.nombre + " Color: " + super.color + " Lado: " + super.base;
	}

	public boolean equals(Object o){
		if(o instanceof Cuadrado){
			return super.equals(o);
		} else return false;
	}
}