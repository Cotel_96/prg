class Figura{

	protected String nombre;
	protected String color;
	protected int coordX, coordY;

	public Figura(String nombre, String color, int x, int y){
		this.nombre = nombre;
		this.color = color;
		this.coordX = x;
		this.coordY = y;
	}

	protected String getNombre() {return this.nombre;}
	protected String getColor() {return this.color;}
	protected int getX() {return this.coordX;}
	protected int getY() {return this.coordY;}

	public double area(){
		return -1;
	}
	public double perimetro(){
		return -1;
	}
	public String toString(){
		return "Nombre: " + this.nombre + " Color: " + this.color;
	}

	public boolean equals(Object o){
		if (o instanceof Figura){
			Figura otra = (Figura)o;
			return this.nombre.equals(otra.getNombre()) &&
					this.color.equals(otra.getColor()) &&
					this.coordX == otra.getX() &&
					this.coordY == otra.getY();
		} else {return false;}

	}	

}