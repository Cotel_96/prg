public class Rectangulo extends Figura{

	protected double base;
	protected double altura;

	public Rectangulo(String nombre, String color, int x, int y, double base, double altura){
		super(nombre,color,x,y);
		this.base = base;
		this.altura = altura;
	}

	public double getBase() {return this.base;}
	public double getAltura() {return this.altura;}

	@Override
	public double area(){
		return base*altura;
	}

	@Override
	public double perimetro(){
		return (base*2)+(altura*2);
	}

	public String toString(){
		return super.toString() + " Base: " + this.base + " Altura: " + this.altura;
	}

	public boolean equals(Object o){
		if(o instanceof Rectangulo){
			Rectangulo otro = (Rectangulo)o;
			return super.equals(o) &&
					this.base == otro.getBase() &&
					this.altura == otro.getAltura();
		}else return false;
	}

}