public class Main{
	public static void main(String[] args) {
		Figura circulo = new Circulo("Circulo","Rojo",0,0,10.0);
		Figura rectangulo = new Rectangulo("Rectangulo","Azul",0,0,4.0,2.0);
		Figura cuadrado = new Cuadrado("Cuadrado","Verde",0,0,5.0);
		Figura cuadrado2 = new Cuadrado("Cuadrado","Verde",0,0,5.0);

		System.out.println(circulo.toString());
		System.out.println(rectangulo.toString());
		System.out.println(cuadrado.toString());
		System.out.println();
		System.out.println("Circulo area: " + circulo.area() + " perimetro: " + circulo.perimetro());
		System.out.println("Rectangulo area: " + rectangulo.area() + " perimetro: " + rectangulo.perimetro());
		System.out.println("Cuadrado area: " + cuadrado.area() + " perimetro: " + cuadrado.perimetro());
		System.out.println();

		System.out.println(cuadrado.equals(circulo));
		System.out.println(cuadrado.equals(rectangulo));
		System.out.println(cuadrado.equals(cuadrado2));

	}
}