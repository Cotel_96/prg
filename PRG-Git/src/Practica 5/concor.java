import java.util.*;
import java.io.*;

public class concor{

  /** Lee desde un fichero binario la Concordancia de un texto.
  * @param fichero String: Nombre del fichero de objetos donde leer.
  */
  public static Concordancia recupera(String fichero){
    try {
      File archivo = new File(fichero);
      FileInputStream fis = new FileInputStream(archivo);
      ObjectInputStream ois = new ObjectInputStream(fis);

      Concordancia recuperado = (Concordancia)ois.readObject();
      ois.close();

      return recuperado;
    } catch (ClassNotFoundException | IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void main(String[] args) {

  String funcionamiento = ("\nConcordancia de un texto.\n\n" + 
    "Parametros:\n\t-o      Indica que el texto esta ordenado\n" +
    "\t-f      Carga la una concordancia guardada en un archivo binario\n\n"+
    "Funcionamiento:\n\tjava concor [parametro] [Nombre fichero]");

    if (args.length == 0 || args.length > 2) {
      System.out.println(funcionamiento);
    } else if(args.length == 1){
      if(args[0].equals("-o") || args[0].equals("-f")){
        System.out.println("Introduce un archivo o un texto!");
      } else {
        try {         
          File file = new File(args[0]);
          Scanner archivo = new Scanner(file);
          Concordancia con = new Concordancia(
            archivo,
            false,
            "[\\p{Space}\\p{Punct}\\p{Digit}¡¿]+");

          System.out.println(con);
          con.almacena(args[0].substring(0, args[0].lastIndexOf('.')) + ".bin");
        } catch (FileNotFoundException e) {
          Concordancia con = new Concordancia(
            args[0],
            false,
            "[\\p{Space}\\p{Punct}\\p{Digit}¡¿]+");

          System.out.println(con);
        }
      }
    } else if(args.length == 2){
      if(!args[0].equals("-o") && !args[0].equals("-f")){
        System.out.println(funcionamiento);
      } else {
        if(args[0].equals("-o")){
          try {
            File file = new File(args[1]);
            Scanner archivo = new Scanner(file);
            Concordancia con = new Concordancia(
              archivo,
              true,
              "[\\p{Space}\\p{Punct}\\p{Digit}¡¿]+");

            System.out.println(con);
            con.almacena(args[1].substring(0, args[1].lastIndexOf('.')) + ".bin");
          } catch (FileNotFoundException e) {
            Concordancia con = new Concordancia(
              args[1],
              true,
              "[\\p{Space}\\p{Punct}\\p{Digit}¡¿]+");
            System.out.println(con);
          }
        } else if(args[0].equals("-f")){
          System.out.println(recupera(args[1]));
        }
      }
    }

  }
}