import lineales.*;
import java.io.*;
import java.util.*;

public class Concordancia implements Serializable{
	
	private NodoCnc prim;
	private int talla;
	private boolean esOrd;
	private String separadores;

	// private NodoCnc fin; 		// No se puede modificar la estructura de la clase

	/** Contructora de una clase Concordancia desde un Scanner
	* @param ent Scanner desde el que se construye la Concordancia
	* @param ord true si la Concordancia esta ordenada ascendentemente
	* @param sep String con la descripción de los separadores de palabras
	*/
	public Concordancia(Scanner ent, boolean ord, String sep){
		this.prim = null;
	    this.talla = 0;
		this.esOrd = ord;
		this.separadores = sep;

		for(int i = 1; ent.hasNext(); i++){
			String linea = ent.nextLine();
			String[] aux = linea.split(this.separadores);
			for(int j=0; j<aux.length; j++){
				if(this.esOrd){
					insOrd(aux[j],i);
				} else {
					insNoOrd(aux[j],i);
				}
			}
		}
	}

	/** Constructora de una clase Concordancia desde un String
	* @param str String desde la que se construye la Concordancia
	* @param ord true si la Concordancia esta ordenada ascendentemente
	* @param sep String con la descripción de los separadores de palabras
	*/
	public Concordancia(String ent, boolean ord, String sep){
		this(new Scanner(ent) , ord, sep);
		// Scanner lectura = new Scanner(ent);
	}

	/** Devuelve el numero de elementos de la Concordancia
	* @return int
	*/
	public int talla(){
		return this.talla;
	}

	/** Devuelve true si los elementos de Concordancia
	*	estan ordenados crecientemente.
	* @return boolean
	*/ 
	public boolean esOrdenada(){
		return this.esOrd;
	}

	/** Devuelve la Concordancia del texto introducido
	* @return String: el contenido de la Concordancia
	*/
	public String toString(){
		String res = "";
		NodoCnc aux = prim;
		for(int i=0; i<talla-1; i++){
			String base = ("%-15s( %5d):" + aux.numLins.toString() + "\n");
			res += String.format(base, aux.palabra, aux.numLins.talla());
			aux = aux.siguiente;
		}
		return res;
	}

	private boolean existe(String pal){
		NodoCnc aux = prim;
		for(int i=0; i<talla; i++){
			if(aux.palabra.equals(pal)) return true;
			else if(aux.siguiente != null) aux = aux.siguiente;
		}
		return false;
	}

	/** Almacena la Concordancia en el fichero de objetos "fichero".
	* @param fichero String: Nombre del fichero de objetos donde almacenar
	* la Concordancia
	*/
	public void almacena(String fichero){
		try {
			File archivo = new File(fichero);
			FileOutputStream fos = new FileOutputStream(archivo);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(this);
			oos.close();
		} catch (IOException e){
			e.printStackTrace();
		}
	}


	/** Inseción en la Concordancia. Una nueva palabra se inserta al final
	*	si no existía previamente o si ya existía se añade la línea en la que
	*	aparece en la información (cola de líneas) asociada a la palabra
	* @param pal String palabra que se quiere introducir
	* @param numLin int numero de linea en el que se quiere introducir
	*/
	private void insNoOrd(String pal, int numLin){
		boolean exists = false;
		if(prim == null){
			this.prim = new NodoCnc(pal,numLin);
			//this.fin = this.prim;
			this.talla++; 
		} else {
			NodoCnc aux = prim;
			exists = this.existe(pal);

			if(exists){
				NodoCnc aux2 = prim;
				NodoCnc ant = aux2;
				boolean encontrado = false;
				for(int i=0; i<talla && !encontrado; i++){
					encontrado = aux2.palabra.equals(pal);
					ant = aux2;
					aux2 = aux2.siguiente;
				}
				ant.numLins.encolar(numLin);
			} else {

				// fin.siguiente = new NodoCnc(pal, numLin);
				// fin = fin.siguiente;
				// this.talla++;

				NodoCnc fin = this.prim;
				for(int i=0; i<this.talla-1; i++){
					fin = fin.siguiente;
				}
				fin.siguiente = new NodoCnc(pal, numLin);
				this.talla++;
			}
		}		
	}

	/** Inserción en la Concordancia. Una nueva palabra se inserta ordenadamente
	*	si no existía previamente o, si ya existía, se añade la línea en la que
	*	aparece en la información (cola de líneas) asociada a cada palabra
	* @param pal String palabra que se quiere introducir
	* @param numLin int numero de linea en el que se quiere introducir
	*/
	private void insOrd(String pal, int numLin){
		boolean exists = false;
		if(prim == null){
			this.prim = new NodoCnc(pal, numLin);
			//this.fin = this.prim;
			this.talla++;
		} else {
			NodoCnc aux = prim;
			exists = this.existe(pal);

			if(exists){
				NodoCnc aux2 = prim;
				NodoCnc ant = aux2;
				boolean encontrado = false;
				for(int i=0; i<talla && !encontrado; i++){
					encontrado = aux2.palabra.equals(pal);
					ant = aux2;
					aux2 = aux2.siguiente;
				}
				ant.numLins.encolar(numLin);
			} else {
				boolean encontrado = false;
				NodoCnc ant = aux;
				for(int i=0; i<talla && !encontrado; i++){
					if (aux == null){
						ant.siguiente = new NodoCnc(pal, numLin,aux);
						
					} else if(aux.palabra.compareTo(pal)<0){
						ant = aux;
						aux = aux.siguiente;
					} else {
						ant.siguiente = new NodoCnc(pal, numLin, aux);
						encontrado = true;
					}
				}
				this.talla++;
			}
		}
	}

}