public class Algoritmos{
/** 
* 	Metodo para ordenar un array de menor a mayor
* 	Seleccion Directa
*/
	public static void selDirecta(int[] v){
		// Se hace un bucle externo que recorre el array entero
		for(int i = 0; i<v.length-1;i++){
			// Se establece que el numero mas pequeño es el indice actual
			int pMin = i;
			// Se crea otro bucle que recorre el array parcialmente y compara el indice actual con el siguiente numero,
			// si ese numero es menor se guarda en pMin
			for(int j=i+1;j<v.length;j++){
				if(v[j]<v[pMin]) pMin = j;
			}
			// Se hacen todos los cambios para dejar al numero en la posicion que toca.
			int aux = v[pMin];
			v[pMin] = v[i];
			v[i] = aux;
		}
	}

/**
*	Metodo para ordenar un array de menor a mayor
*	Inserción Directa
*/
	public static void insDirecta(int[] v){
		for(int i = 0; i<=v.length-1;i++){
			int x = v[i]; // Elemento a insertar
			int j = i-1; // Inicio de la parte ordenada
			while(j>=0 && v[j]>x){
				v[j+1] = v[j];
				j--;
			}
			v[j+1] = x; // Asignar x a la parte ordenada 
		}
	}

/**
*	Metodo para ordenar un array de menor a mayor
*	Intercambio directo o burbuja
*/
	public static void burbuja(int[] v){
		for(int i = 1; i<=v.length-1;i++){
			for(int j=0;j<v.length-i;j++){
				// Se comparan pares de elementos consecutivos
				if(v[j]>v[j+1]){
					// Si el par esta desordenado entonces lo intercambia
					int x = v[j];
					v[j] = v[j+1];
					v[j+1] = x;
				}
			}
		}
	}

/**
*	Metodo para ordenar un array a partir de dos arrays ordenados
*	Mezcla Natural
* 	a y b estan ordenados
*	c e a.length + b.length
*	
*/
	public static void mezclaNatural(int a[],int b[],int[] c){
		int i = 0, l = a.length, j= 0, m=b.length, k=0;

		while(i<1 && j<m){
			if(a[i] < b[j]){c[k] = a[i];i++;}
			else{c[k] = b[j]; j++;}
			k++;
		}
		for(int r=i; r<1;r++){c[k] = a[r];k++;}
		for(int r=j; r<m;r++){c[k] = b[r];k++;}
	}

/**
*	Metodo para ordenar un array de menor a mayor
*	Merge Sort
*	Divide el array a la mitad de forma recursiva hasta que lo tiene ordenado
*	REVISAR PORQUE NO FUNCIONA!
*/
	public static void mergeSort(int[] v, int ini, int fin){
		if(ini<fin){
			int mitad = (fin+ini)/2;
			mergeSort(v,ini,mitad);
			mergeSort(v,mitad+1,fin);	
			//mezclaNatural(v,ini,mitad,fin);		
		}
	}
/**
*	Merge Sort de forma que se entienda
*	Algoritmo divide y venceras
*/
	public static int[] mergeSortIt(int[] a){
		if(a.length == 1) return a;				//Condición del fin de la recursividad
		int medio = a.length/2;					//Posicion del centro de la lista

		// Creamos 2 listas
		int[] lista1 = new int[medio];			
		int[] lista2 = new int[a.length - medio];

		// Copiamos los elementos de a, la mitad en 1 la otra en 2
		for(int i=0;i<medio;i++){
			lista1[i] = a[i];
		}
		for(int i=medio;i<a.length;i++){
			lista2[i-medio] = a[i];
		}

		// Se llama a la funcion de forma recursiva
		int[] res1 = mergeSortIt(lista1);
		int[] res2 = mergeSortIt(lista2);
		int[] resFinal = a;						//Reutilizamos a

		// Fusionamos las listas
		int n1=0,n2=0;
		for(int i=0;i<a.length;i++){
			if(n1==res1.length){				//No hay mas elementos en res1
				resFinal[i] = res2[n2];
				++n2;
			}else if(n2==res2.length){			//No hay mas elementos en res2
				resFinal[i] = res1[n1];
				++n1;
			}else{								//Hay elementos en ambas listas
				//Comparamos los elementos de las posiciones a analizar
				if(res1[n1]<res2[n2]){			//Si el menor esta en res1 utilizar el mismo
					resFinal[i]=res1[n1];
					++n1;
				}else{							//Si el menor esta en res2 utilizar el mismo
					resFinal[i]=res2[n2];
					++n2;
				}
			}
		}
		return resFinal;
	}

/**
* 	Metodo para facilitar la impresión de Arrays -.-"
* 	En Python esto no pasa
*/
	public static void toString(int[] v){
		System.out.print("[");
		for(int i=0;i<v.length;i++){
			if(i==v.length-1) System.out.print(v[i]);
			else System.out.print(v[i] + ",");
		}
		System.out.print("]");
	}

	public static void main(String[] args) {
		int[] prueba = {12,24,1,5,89};
		int[] prueba1 = {6,7,8,9,10};
		int[] prueba2 = {1,2,3,4,5};
		int[] prueba3 = new int[10];
		// mezclaNatural(prueba1,prueba2,prueba3);
		// toString(prueba1);
		// toString(prueba2);
		// toString(prueba3);
		toString(mergeSortIt(prueba));

	}
}