
@SuppressWarnings("serial")
public class InputFormatException extends Exception {
	public InputFormatException(String info){
		super(info);
	}
}
