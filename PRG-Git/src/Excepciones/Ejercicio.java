import java.util.*;


public class Ejercicio {
	public static final int AZUL = 0;
	public static final int VERDE = 1;
	public static final int ROJO = 2;
	public static final int AMARILLO = 3;
	
	private static final String[] colores = {"AZUL","VERDE","ROJO","AMARILLO"};
	
	public static int leerRango(int ini, int fin){
		Scanner teclado = new Scanner(System.in);
		int res = -1;
		do{
			try{
				System.out.print("Introduce un numero entre " + ini + " y " + fin +": ");
				res = teclado.nextInt();
				if(res < ini || res > fin){
					throw new InputFormatException("El numero no esta en el intervalo, prueba otra vez");
				}
			
			}catch (InputFormatException e){
				System.out.println(e.getMessage());
			}catch (InputMismatchException e){				
				System.out.print("Entrada no valida, prueba otra vez");
				teclado.nextLine();
				System.out.println();
			}catch (NegativeArraySizeException e){
				System.out.println("El numero no puede ser negativo");
				System.out.println();
			}
		}while(res < ini || res > fin);
		teclado.close();
		return res;		
	}
	
	public static String leerColor(){
		Scanner teclado = new Scanner(System.in);
		String res = null;
		do{
			try{
				System.out.print("Introduce un color entre " + Arrays.toString(colores) + ": ");
				res = teclado.nextLine();
				res = res.toUpperCase();
				boolean cond = false;
				for(int i = 0; i<colores.length;i++){
					if(res.equals(colores[i])) cond = true;
				}
				if(cond == false){
					throw new ColorNotValidException("La entrada no es valida, prueba otra vez");
				}
			}catch (ColorNotValidException e){
				System.out.println(e.getMessage());
				res = null;
				System.out.println();
			}catch (InputMismatchException e){
				System.out.println("La entrada no es valida, prueba otra vez");
				teclado.nextLine();
				System.out.println();
			}
		}while(res == null);
		teclado.close();
		return res;
	}
	
	
	
	
}
