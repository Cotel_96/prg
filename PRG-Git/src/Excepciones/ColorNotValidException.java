
@SuppressWarnings("serial")
public class ColorNotValidException extends Exception {
	public ColorNotValidException(String info){
		super(info);
	}
}
