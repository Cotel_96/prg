/**
 * Clase Banco para representar un conjunto de cuentas.
 * @author PRG 
 * @version Curso 2014/15
 */
import java.util.*;
import java.io.*;

public class Banco implements Serializable {
    
    /** Array de objetos Cuenta. */
    public Cuenta[] cuentas;
    /** Número actual de cuentas y primer índice disponible en el array cuentas. */
    private int numCuentas;
    /** Número máximo de cuentas. */
    public static final int MAX_CUENTAS = 100;

    /**
     * Crea un banco sin cuentas (puede tener un máximo de MAX_CUENTAS cuentas).
     */
    public Banco() {
        this.cuentas = new Cuenta[MAX_CUENTAS];
        this.numCuentas = 0;
    }

    /**
     * Consultor del número actual de cuentas.
     * @return int, número actual de cuentas.
     */
    public int getNumCuentas() { return numCuentas; }
    
    /**
     * Permite añadir la Cuenta c al banco. Si el banco está completo,
     * duplica el número de cuentas que pueden haber en el banco.
     * @param c Cuenta a añadir.
     */
    public void añadir(Cuenta c) {
        if (numCuentas >= cuentas.length) duplica();   
        cuentas[numCuentas++] = c;
    }
    
    /**
     * Duplica el tamaño del array cuentas.
     */
    private void duplica() {
        Cuenta[] aux = new Cuenta[2*cuentas.length];
        for (int i = 0; i < cuentas.length; i++) aux[i] = cuentas[i];
        cuentas = aux;
    }

    /**
     * Devuelve la cuenta con número de cuenta n.
     * Si tal cuenta no existe, devuelve null.
     * @param n int que indica el número de cuenta.
     * @return Cuenta, la cuenta resultado.
     */
    public Cuenta getCuenta(int n) {
        int i = 0;
        while (i < numCuentas && cuentas[i].getNumCuenta() != n) i++;
        if (i < numCuentas) return cuentas[i];
        else return null;
    }

    public void cargarFormatoTexto(Scanner f){
        Cuenta nueva;
        int errores = 0;                    
        while(f.hasNext()){
            try {                
                String linea = f.next();
                int numero = Integer.parseInt(linea);
                if(String.valueOf(numero).length() > 5){
                    throw new IllegalArgumentException("El numero no puede tener mas de 5 digitos!");
                }
                linea = f.next();
                double saldo = Double.parseDouble(linea);
                if(saldo < 0) {
                    throw new IllegalArgumentException("El saldo no puede ser negativo!");
                }
                nueva = new Cuenta(numero,saldo);
                if(getCuenta(numero) == null){
                    añadir(nueva);
                }
            } catch (InputMismatchException e){
                System.out.println("El formato es incorrecto!");
                f.nextLine();
                errores++;                           
            } catch (NumberFormatException e){
                System.out.println("El formato es incorrecto!");
                f.nextLine();
                errores++;                
            } catch (IllegalArgumentException e){
                System.out.println(e.getMessage());
                f.nextLine();
                errores++;
            }
        }
        f.close();
        if(errores != 0){
            System.out.println("Detectados " + errores + " errores en el archivo.");
            System.out.println();
        }
               
    }

    public void guardarFormatoTexto(PrintWriter f){
        for(int i=0;i<this.numCuentas;i++){
            f.println(this.cuentas[i].toString());
        }
        f.close();
        
    }

    public void guardarFormatoObjeto(ObjectOutputStream f) throws IOException {        
        
        for(int i=0; i<this.numCuentas;i++){
            f.writeObject(this.cuentas[i]);
        }
        // Para escribir los objetos Cuenta

        // f.writeObject(this); // Para escribir el objeto Banco

        try {
            f.close();
        } catch (IOException e){
            System.out.println("Hubo un problema al cerrar el flujo!");
            System.out.println();
        }
    }

    public void cargarFormatoObjeto(ObjectInputStream f) throws IOException, ClassNotFoundException {
        
        Cuenta nueva;
        try {
            while (true){
                nueva = (Cuenta)(f.readObject());
                if(getCuenta(nueva.getNumCuenta()) == null){
                    this.añadir(nueva);
                }
            }
        } catch (EOFException e) {
            System.out.println("Fin del archivo.");
            System.out.println();
        } finally {
            try {
                f.close();
            } catch (IOException e){
                System.out.println("Hubo un problema al cerrar el flujo!");
                System.out.println();
            }
        }

        // Banco b;        
        // b = (Banco)(f.readObject());
        // for(int i=0;i<b.getNumCuentas();i++){
        //     if(getCuenta(b.cuentas[i].getNumCuenta()) == null){
        //         this.añadir(b.cuentas[i]);
        //     }
        // }
        

        
    }

    /**
     * Devuelve una String con toda la información del banco.
     * El formato es una cuenta por línea.
     * @return String.
     */
    public String toString() {
        if (numCuentas == 0) return "No hay cuentas en el banco";
        else {
            String res = "";
            for (int i = 0; i < numCuentas; i++) res += cuentas[i] + "\n"; 
            return res;
        }        
    }
}
